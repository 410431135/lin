# 利用Google Form + Slide 實現自動發送研習證明

2021.8.21

## 目的

利用Google form 讓對象填寫表單，包含電子信箱等資訊。送出後，該對象會收到一份主辦方製作的PDF，內含有該對象的相關資料。

## 使用工具

Google form, goole spreadsheet, google slide, app script (gs), html (optional), an account @gmail.com

## 設置筆記

建立Google Form，至少包含「Email」欄位。從該表單建立試算表。

進到該試算表後，點選App script。

使用html的方式參考[這個連結](https://www.letswrite.tw/gas-auto-epaper/)。

以下介紹配合google slide 做出自訂的PDF。

想法是這樣：先建立一個slide template，當使用者提交表單的時候，將該樣板複製一份，將表單的資料填入相對應的位置，最後轉成PDF寄出。

### 建立Slide template

到Google drive 開啟一個資料夾，建立一個新的slide。並自訂裡面的樣式。需要依照使用者資料改寫的地方，用特殊的符號來當作placeholder，例如：``{{name}}``、`{{email}}`

記下該簡報的id、該資料夾的id。

### code.gs

```go
function onSubmit(datas){
  var data = datas.namedValues;
  var replyEmail = data['email'][0];
  var name = data['name'][0];

  // Duplicate the template presentation using the Drive API.
  var templatePresentationId = '此處改成簡報id';
  var copyTitle = name + '_測試'; //名稱可自訂
  var copyFile = {
    title: copyTitle,
    parents: [{id: '此處改成資料夾id'}] //folder's id
  };
  copyFile = Drive.Files.copy(copyFile, templatePresentationId);
  var presentationCopyId = copyFile.id


  // Create the text merge (replaceAllText) requests for this presentation.
  requests = [{
    replaceAllText: {
      containsText: {
        text: '{{name}}',
        matchCase: true
      },
      replaceText: name
    }
  }, {
    replaceAllText: {
      containsText: {
        text: '{{email}}',
        matchCase: true
      },
      replaceText: replyEmail
    }
  }];
  
  // Execute the requests for this presentation.
  var result = Slides.Presentations.batchUpdate({
    requests: requests
  }, presentationCopyId);

  MailApp.sendEmail({
    to: replyEmail,
    subject: "自訂信件主旨",
    attachments:[DriveApp.createFile(DriveApp.getFileById(presentationCopyId))]
  })
}
```

### 設置自動化

到app script 設置觸發，設置為提交表單時。如此，有人提交表單就會執行上述動作。