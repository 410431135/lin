# GitBook Plugins

這邊紀錄一下用到的插件：

Ref : http://gitbook.zhangjikai.com/plugins.html#puml

## book.json

```json
{
	"plugins": [
		"page-toc",
		"katex",
		"-lunr",
		"-search",
		"search-plus",
		"splitter",
		"code",
		"expandable-chapters-small",
		"puml",
		"disqus",
		"back-to-top-button",
		"hide-element",
		"graph",
		"chart",
		"alerts",
		"klipse",
		"pageview-count"
	],
	"pluginsConfig": {
    	"disqus": {
		"shortName": "https-410431135-gitlab-io-django-note"
		},
		"hide-element": {
			"elements": [".gitbook-link"]
		}
	}
}
```



## plant uml

```json
{
    "plugins": ["puml"]
}
```

範例：

```
{% plantuml %}
Class Stage
    Class Timeout {
        +constructor:function(cfg)
        +timeout:function(ctx)
        +overdue:function(ctx)
        +stage: Stage
    }
    Stage <|-- Timeout
{% endplantuml %}
```

Output:

{% plantuml %}
Class Stage
    Class Timeout {
        +constructor:function(cfg)
        +timeout:function(ctx)
        +overdue:function(ctx)
        +stage: Stage
    }
    Stage <|-- Timeout
{% endplantuml %}

## graph

[官網](https://mauriciopoppe.github.io/function-plot/)

```
{% graph %}
{
    "title": "1/x * cos(1/x)",
    "grid": true,
    "xAxis": {
        "domain": [0.01, 1]
    },
    "yAxis": {
        "domain": [-100, 100]
    },
    "data": [{
        "fn": "1/x * cos(1/x)",
        "closed": true
    }]
}
{% endgraph %}
```

Output:

{% graph %}
{
    "title": "1/x * cos(1/x)",
    "grid": true,
    "xAxis": {
        "domain": [0.01, 1]
    },
    "yAxis": {
        "domain": [-100, 100]
    },
    "data": [{
        "fn": "1/x * cos(1/x)",
        "closed": true
    }]
}
{% endgraph %}

## chart

[c3.js](https://github.com/c3js/c3)

```
{% chart %}
{
    "data": {
        "type": "bar",
        "columns": [
            ["data1", 30, 200, 100, 400, 150, 250],
            ["data2", 50, 20, 10, 40, 15, 25]
        ],
        "axes": {
            "data2": "y2"
        }
    },
    "axis": {
        "y2": {
            "show": true
        }
    }
}
{% endchart %}
```

{% chart %}
{
    "data": {
        "type": "bar",
        "columns": [
            ["data1", 30, 200, 100, 400, 150, 250],
            ["data2", 50, 20, 10, 40, 15, 25]
        ],
        "axes": {
            "data2": "y2"
        }
    },
    "axis": {
        "y2": {
            "show": true
        }
    }
}
{% endchart %}

## alert

```
Info styling
> **[info] For info**
>
> Use this for infomation messages.

Warning styling
> **[warning] For warning**
>
> Use this for warning messages.

Danger styling
> **[danger] For danger**
>
> Use this for danger messages.

Success styling
> **[success] For info**
>
> Use this for success messages.
```

Info styling
> **[info] For info**
>
> Use this for infomation messages.

Warning styling
> **[warning] For warning**
>
> Use this for warning messages.

Danger styling
> **[danger] For danger**
>
> Use this for danger messages.

Success styling
> **[success] For info**
>
> Use this for success messages.

## Klipse

[官網](https://github.com/viebel/klipse)

klipse 目前支持下面的语言：

- javascript: evaluation is done with the javascript function eval and pretty printing of the result is done with pretty-format
- clojure[script]: evaluation is done with Self-Hosted Clojurescript
- ruby: evaluation is done with Opal
- C++: evaluation is done with JSCPP
- python: evaluation is done with Skulpt
- scheme: evaluation is done with BiwasScheme
- PHP: evaluation is done with Uniter
- BrainFuck
- JSX
- EcmaScript2017
- Google Charts: See Interactive Business Report with Google Charts.

```
{% klipse "eval-python" %}
print([x**2 for x in range(10)])
{% endklipse %}
```

{% klipse "eval-python", loopMsec="1000" %}
print([x**2 for x in range(10)])
{% endklipse %}

> **[info] Note**
>
> 如果沒有出現結果，按`ctrl+Enter`

## 打數學

```latex
$$\phi \cos \frac{2}{3}$$
```

$$\phi \cos \frac{2}{3}$$

## PDF

```
{% pdf src="https://gitlab.com/410431135/mathteacherlin/raw/master/數學大考題/國中數學/108教育會考數學科/108P_Math150DPI.pdf?inline=false", width="100%", height="850" %}
{% endpdf %}
```

{% pdf src="https://gitlab.com/410431135/mathteacherlin/raw/master/數學大考題/國中數學/108教育會考數學科/108P_Math150DPI.pdf?inline=false", width="100%", height="850" %}{% endpdf %}

## Google Docs

https://github.com/gramener/gitbook-plugin-googledocs

```
[some text](http:// ~~~)
```

##Plant uml 可用中文版

```uml
@startuml
a -> b
我 -> 愛你
@enduml
```

