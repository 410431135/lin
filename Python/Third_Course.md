# Third Course

這堂課要教python重要的容器：`list`（串列）、String slicing；

以及函數的概念。

## List

Python有許多「容器」可以用來儲存資料，其中一個就是串列。

```python
some_list = ['a', 1, 'abc', '123', '水餃']
print(some_list[0]) # 'a'
print(some_list[1]) # 1
print(some_list[2]) # 'abc'
print(some_list[3]) # '123'
print(some_list[4]) # '水餃'
print(some_list[-1]) # 'a'
```

串列在python裡面，使用中括號`[]`來表示。範例中的`some_list`裝了`'a', 1, 'abc', '123', '水餃'`等五樣東西。

包含有單一個字母、整數、字串、文字等等。這是python特別的地方，他可以裝任何東西。

註：在多數的程式語言中，必須同一種型態才能裝一起。

Python與多數程式語言相同，索引從0開始標號，所以容器中第一個東西，在python中是第0個，依此類推。而負代表從最後往回算。

###Quick Exercise

配合第二堂課學到的`random`模組，建立一個決定吃什麼東西的程式。

假設待選擇的食物有：義大利麵、牛排、鴨肉飯、鍋貼。

請讓程式可以隨機選擇一個，來解決午餐要吃什麼的困擾！

Hint：`random.choice()` or `random.randint(int start,int end)`

##String slicing

```python
some_string = 'Hello, World!'
print(some_string[0]) # 'H'
print(some_string[-1]) # '!'
print(some_string[2:4]) # 'll' 
print(some_string[-1:-3]) # ''
print(some_string[-1:-3:-1]) # '!d'
print(some_string[-3:-1]) # 'ld'
print(some_string[0:5:2]) # 'Hlo'
print(some_string[::-1]) # '!dlroW ,olleH'
```

{% klipse "eval-python" %}
some_string = 'Hello, World!'
print(some_string[::-1]) 
{% endklipse %}

`string[起始:終止:間隔]`

起始省略不寫，預設為0。終止省略不寫，預設到最後。間隔不寫，預設為1。

可以注意第四行，[2:4]意思是從第2個開始（記得，從第0開始計算），到第四個之前的都取。

也就是取的字母是 起始≤字母們<終止。

第六行，若要反方向，將間隔設定為-1即可。

### Transform

有需要時，我們可以將字串轉為list

```python
name = 'Kyle'
list(name)
# output: ['K', 'y', 'l', 'e']
```

### Exercise

[a020: 身分證檢驗](https://zerojudge.tw/ShowProblem?problemid=a020)

## Function define and call

從First Course 開始，我們學了很多函數，包含`print()`、`input()`、`help()`、`type()`、`random()`等。

函數可以讓我們把某件事情定義出來，統合好幾個步驟，變成一個簡單的指令。

一般來說，有內建函數（built-in functions）、外部函數（需要import 才可使用）、自訂函數。

例如，我們想要輸入一組成績，國英數自社，然後讓函數幫我們算出算術平均數。

```python
# 定義函數，用關鍵字「def」
# 括號內輸入要有哪些參數
def average(chinese, english, math, science, socity):
    # 平均＝五科總和 ÷ 5
    average_score = (chinese + english + math + science + socity)/5.0
    # 想想看為何要除以5.0，而非5？
    return average_score

score = average(60, 70, 80, 90, 100)
print("Your average score = %f", % score)
# 80.0
```

{% klipse "eval-python" %}
def average(chinese, english, math, science, socity):
    average_score = (chinese + english + math + science + socity)/5.0
    return average_score
score = average(60, 70, 80, 90, 100)
print("Your average score = %.2f" % score)
{% endklipse %}

從這裡我們可以觀察一下函數定義時的架構：

```python
def 函數名稱(需要使用者輸入的參數們):
    這個函數要做的事情
    這個函數要做的事情
    return 回傳什麼
```

### Exercise

試試看將身分證檢驗做成函數吧！

### Exercise

寫一個程式，讓使用者輸入一個正整數，程式回傳它所有的正因數。

## Advance Exercise

製作一個簡易的文字遊戲，玩家角色原本在原點（0,0）的位置。

玩家可以輸入 up, down, left, right，或是簡寫 U,D,L,R，然後玩家角色就會向上、下、左、右移動一格。

```bash
> 輸入指令：
> U
> 角色移動至(0,1)
> 輸入指令：
> L
> 角色移動至(-1,1)
```

