# 110學年度第二學期--Python微課程--第二期課程資訊

## Line 群組 QR Code

![IMG_2674](IMG_2674.JPG)

## 上課使用的資源

1. [Google Colab](https://colab.research.google.com)
2. [交作業](https://docs.google.com/spreadsheets/d/1Vq4pOyyiMJMPteR-372swjfc6Aqfu0-hcom_z36gfcY/edit?usp=sharing)
3. [Zero Judge](https://colab.research.google.com)
4. [Solo Learn](https://www.sololearn.com)
5. [W3 School](http://w3schools.com)